from flask import Flask, jsonify,abort,make_response,request
import random

app = Flask(__name__)
FILE_PATH = 'storage.txt'


@app.route("/read", methods=['GET'])
def get_phrase():
    # readlines
    phrase_file = open(FILE_PATH, 'r')
    file_data = phrase_file.readlines()
    phrases = list()
    for c, line in enumerate(file_data, 1):
        # add lines to list
        phrases.append({
                'id': c,
                'phrase': line[:-1]
            })
    phrase_file.close()
    return jsonify({'phrases': phrases})

@app.route('/write', methods=['POST'])
def create_task():
    request_data = request.json
    if not (request_data or 'phrase' in request_data):
        abort(400)

    phrase_file = open(FILE_PATH, "r")
    file_lines = phrase_file.readlines()
    lines = list()
    for line in file_lines:
        lines.append(line)
    else:
        # append request data to file
        lines.append(request_data.get('phrase') + "\n")

    # write data to file
    write_file_from_list(FILE_PATH, lines)
    phrase_file.close()

    return jsonify({'id': str(len(lines))}), 201

def write_file_from_list(file_obj, line_list):
    """
    Writes in a file object from a given list
    :param file_obj: Type File obj, File object in which data needs to be written
    :param line_list: Type list, List of lines which needs to be written in file
    """
    file_obj = open(FILE_PATH, 'w')
    file_data = str()
    for line in line_list:
        file_data = file_data + line
    file_obj.write(file_data)

@app.route('/delete/<int:id>', methods=['DELETE'])
def delete_task(id):
    # readlines
    phrase_file = open(FILE_PATH, 'r')
    file_data = phrase_file.readlines()

    lines = list()
    deleted = False
    # delete line from list if exists
    for c, line in enumerate(file_data, 1):
        if c == id:
            deleted = True
            continue
        lines.append(line)

    if deleted:
        # if data deleted update file
        write_file_from_list(FILE_PATH, lines)
        response = jsonify({'success': True})
    else:
        # if line-number not found
        response = jsonify(
            {
                'success': False,
                'error': 'Line with number {} not found'.format(id)
            }
        )        

    return response  

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


if __name__ == "__main__":
	app.run(debug=True)